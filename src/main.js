import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import Gameplay from './views/Gameplay.vue'
import StartPage from './views/StartPage.vue'
import GameOver from './views/GameOver.vue'

Vue.config.productionTip = false
Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: StartPage
  },
  {
    path: '/gameplay',
    component: Gameplay
  },
  {
    path: '/gameover',
    component: GameOver,
    props: true
  }
]

const router = new VueRouter({routes});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
